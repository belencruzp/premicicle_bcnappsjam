//
//  BiBcnAppAppDelegate.h
//  BiBcnApp
//
//  Created by gJurado on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrincipalViewController.h"

@interface BiBcnAppAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) PrincipalViewController *pvc;


@end
