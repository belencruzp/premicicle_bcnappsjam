//
//  main.m
//  BiBcnApp
//
//  Created by gJurado on 12/6/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BiBcnAppAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BiBcnAppAppDelegate class]));
    }
}
