//
//  LibroAnnotation.h
//  CoreLocationTutorial
//
//  Created by gJurado on 12/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LibroAnnotation : NSObject <MKAnnotation>
{
    CLLocationCoordinate2D *coordinate;
    NSString *title;
    NSString *subtitle;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;


-(id)initWithDictionary:(NSDictionary *)dict;






@end
