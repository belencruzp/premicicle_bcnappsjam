//
//  BusquedaViewController.h
//  BiBcnApp
//
//  Created by gJurado on 12/15/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BusquedaViewController : UIViewController 


- (IBAction)filtroLibro:(id)sender;
- (IBAction)filtroBibliotecas:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *busqueda;
@property (weak, nonatomic) IBOutlet UITableView *tablaBusqueda;
@property (weak, nonatomic) IBOutlet UITableViewCell *celdaBusqueda;


@end
