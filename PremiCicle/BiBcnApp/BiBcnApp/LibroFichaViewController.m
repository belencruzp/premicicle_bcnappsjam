//
//  LibroFichaViewController.m
//  BiBcnApp
//
//  Created by gJurado on 12/15/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "LibroFichaViewController.h"

@implementation LibroFichaViewController
@synthesize scrollView;
@synthesize contentView;
@synthesize titulo;
@synthesize imagen;
@synthesize autor;
@synthesize descripcion;
@synthesize status;
@synthesize fecha;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}



#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.scrollView.contentSize = self.contentView.frame.size;
}


- (void)viewDidUnload
{
    [self setTitulo:nil];
    [self setImagen:nil];
    [self setAutor:nil];
    [self setDescripcion:nil];
    [self setStatus:nil];
    [self setFecha:nil];
    [self setScrollView:nil];
    [self setContentView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)reservar:(id)sender {
}

- (IBAction)afegirFavorits:(id)sender {
}

- (IBAction)afegirABooksharing:(id)sender {
}
@end
