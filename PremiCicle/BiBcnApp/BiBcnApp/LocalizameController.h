//
//  LocalizameController.h
//  BiBcnApp
//
//  Created by gJurado on 12/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocalizameController : UIViewController <CLLocationManagerDelegate, MKMapViewDelegate>


@property (nonatomic) int tipoAnnotation;
@property (weak, nonatomic) IBOutlet MKMapView *mapaLocalizame;
@property (nonatomic) CLLocationCoordinate2D coordenadaBusqueda;

- (IBAction)busquedaPorBibliotecas:(id)sender;
- (IBAction)busquedaPorLibros:(id)sender;
- (IBAction)busquedaPorOtros:(id)sender;

@end
