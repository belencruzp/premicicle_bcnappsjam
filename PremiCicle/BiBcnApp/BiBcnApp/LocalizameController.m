//
//  LocalizameController.m
//  BiBcnApp
//
//  Created by gJurado on 12/10/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "LocalizameController.h"
#import "LibroAnnotation.h"
#import "BibliotecaAnnotation.h"


@implementation LocalizameController
@synthesize mapaLocalizame;
@synthesize tipoAnnotation = _tipoAnnotation;
@synthesize coordenadaBusqueda = _coordenadaBusqueda;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

-(void)setMapaConAnnotation:(int)tipoAnnotation //1 si es Bibliotecas 2 si es Libro
{
    NSBundle *bibliotecasPlist;
    NSBundle *librosPlist;
    NSString *path;
    
    if(tipoAnnotation == 1)
    {
        bibliotecasPlist = [NSBundle mainBundle];
        path = [bibliotecasPlist pathForResource:@"bibliotecasPlist2" ofType:@"plist"];
    }
    else
    {
        librosPlist = [NSBundle mainBundle];
        path = [librosPlist pathForResource:@"librosPlist" ofType:@"plist"];
    }
    
    NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:path];
    if(dic)
    {
        NSArray *array = [dic objectForKey:@"Root"];
        if(array)
        {
            if(tipoAnnotation == 1)
            {
                for(NSDictionary *annotations in array)
                {
                    BibliotecaAnnotation *ba = [[BibliotecaAnnotation alloc]initWithDictionary:annotations];
                    [self.mapaLocalizame addAnnotation:ba];
                }
            }
            else
            {
                for(NSDictionary *annotations in array)
                {
                    LibroAnnotation *la = [[LibroAnnotation alloc]initWithDictionary:annotations];
                    [self.mapaLocalizame addAnnotation:la];
                }
            }
        }
        else
            NSLog(@"Error retrieving arrayB");
        
        
    }else
        NSLog(@"Error retrieving biblios");
    
}


-(void)setMapaInicial
{
    self.mapaLocalizame.delegate = self;
    MKCoordinateRegion region; 
    
    double minLat = 41.412286;
    double maxLat = 41.41602;
    double minLon = 2.175021;
    double maxLon = 2.216563;
    
    region.center.latitude = (maxLat + minLat)/2;
    region.center.longitude = (maxLon + minLon)/2;
    region.span.latitudeDelta = maxLat - minLat;
    region.span.longitudeDelta = maxLon - minLon;
    
    [self.mapaLocalizame setRegion:region animated:YES];
    [self setMapaConAnnotation:1];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setMapaInicial];
}

- (void)viewDidUnload
{
    [self setMapaLocalizame:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)busquedaPorBibliotecas:(id)sender 
{
    [self setMapaConAnnotation:1];
    
//    //practica para mover el mapa justo la mitad de su anchura a la izquierda
//    CLLocationCoordinate2D mapCenter = mapaLocalizame.centerCoordinate;
//    mapCenter = [mapaLocalizame convertPoint:
//                 CGPointMake(1, (mapaLocalizame.frame.size.height/2.0))
//                   toCoordinateFromView:mapaLocalizame];
//    [mapaLocalizame setCenterCoordinate:mapCenter animated:YES];
}

- (IBAction)busquedaPorLibros:(id)sender 
{
    [self setMapaConAnnotation:2];
}

- (IBAction)busquedaPorOtros:(id)sender 
{  
    MKCoordinateRegion theRegion = mapaLocalizame.region;
    
    // Zoom out
    theRegion.span.longitudeDelta *= 2.0;
    theRegion.span.latitudeDelta *= 2.0;
    [mapaLocalizame setRegion:theRegion animated:YES];
}

#pragma mark Annotation delegate methods

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    if ([annotation isKindOfClass:[LibroAnnotation class]])
    {
        // Try to dequeue an existing pin view first.
        MKPinAnnotationView*    pinView = (MKPinAnnotationView*)[mapView
                                                                 dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
        if (!pinView)
        {
            // If an existing pin view was not available, create one.
            pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                      reuseIdentifier:@"CustomPinAnnotationView"];
            pinView.image = [UIImage imageNamed:@"LibroIcono.png"];
            pinView.canShowCallout = YES;
            
            // Add a detail disclosure button to the callout.
            UIButton* rightButton = [UIButton buttonWithType: UIButtonTypeInfoLight];
            pinView.rightCalloutAccessoryView = rightButton;
            pinView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"LibroIcono.png"]];
        }
        else
        {
            pinView.annotation = annotation;
        }
        return pinView;
    }
    else
    {
        if([annotation isKindOfClass:[BibliotecaAnnotation class]])
        {
            // Try to dequeue an existing pin view first.
            MKPinAnnotationView*    pinView = (MKPinAnnotationView*)[mapView
                                                                     dequeueReusableAnnotationViewWithIdentifier:@"CustomPinAnnotationView"];
            if (!pinView)
            {
                // If an existing pin view was not available, create one.
                pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:@"CustomPinAnnotationView"];
                pinView.image = [UIImage imageNamed:@"BibliotecaIcono.png"];
                pinView.canShowCallout = YES;
                
                UIButton* rightButton = [UIButton buttonWithType: UIButtonTypeInfoLight];
                pinView.rightCalloutAccessoryView = rightButton;
                pinView.leftCalloutAccessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"libroIcono.png"]];
            }
            else
            {
                pinView.annotation = annotation;
            }
            return pinView;
        }
    }
    return  nil;
}





@end
