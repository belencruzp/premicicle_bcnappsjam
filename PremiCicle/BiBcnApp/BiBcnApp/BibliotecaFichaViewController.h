//
//  BibliotecaFichaViewController.h
//  BiBcnApp
//
//  Created by gJurado on 12/15/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BibliotecaFichaViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (weak, nonatomic) IBOutlet UITextView *descripcion;
@property (weak, nonatomic) IBOutlet UIImageView *imagen;


@property (weak, nonatomic) IBOutlet UIButton *irAMapa;



@end
