//
//  LoginViewController.m
//  BiBcnApp
//
//  Created by gJurado on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"

@implementation LoginViewController

@synthesize btn_Login;
@synthesize tbc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

}

- (void)viewDidUnload
{
    [self setBtn_Login:nil];
    [super viewDidUnload];
       // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)uiViewLoginAlert
{
    UIAlertView *lp = [[UIAlertView alloc] initWithTitle:@"Login" message:@"Ingrese su login y contrasena" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Ingresar" , nil];
    lp.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    [lp show];
}

-(IBAction)animacion:(UIButton *)sender
{
    //Animacion del boton
    [UIView animateWithDuration:1.5 animations:^{
        CGAffineTransform moveCenter = CGAffineTransformMakeTranslation(0.0, 128.0);
        [self.btn_Login setCenter: CGPointApplyAffineTransform(self.btn_Login.center, moveCenter)];
    }];
    //[self uiViewLoginAlert];
    [NSTimer scheduledTimerWithTimeInterval:1.8 target:self selector:@selector(uiViewLoginAlert) userInfo:nil repeats:NO];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        NSLog(@"Login: %@",[[alertView textFieldAtIndex:0]text]);
        NSLog(@"Password: %@",[[alertView textFieldAtIndex:1]text]);
    }
    [self performSegueWithIdentifier:@"Ingreso" sender:self];
    
}


@end
