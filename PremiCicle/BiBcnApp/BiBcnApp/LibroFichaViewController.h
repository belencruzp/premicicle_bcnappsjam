//
//  LibroFichaViewController.h
//  BiBcnApp
//
//  Created by gJurado on 12/15/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LibroFichaViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (weak, nonatomic) IBOutlet UILabel *titulo;
@property (weak, nonatomic) IBOutlet UIImageView *imagen;
@property (weak, nonatomic) IBOutlet UITextView *autor;
@property (weak, nonatomic) IBOutlet UITextView *descripcion;


@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *fecha;

- (IBAction)reservar:(id)sender;
- (IBAction)afegirFavorits:(id)sender;
- (IBAction)afegirABooksharing:(id)sender;


@end
