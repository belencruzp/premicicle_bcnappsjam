//
//  LoginViewController.h
//  BiBcnApp
//
//  Created by gJurado on 12/7/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIButton *btn_Login;

@property (weak,nonatomic) IBOutlet UITabBarController *tbc;

- (IBAction)animacion:(UIButton *)sender;

@end
