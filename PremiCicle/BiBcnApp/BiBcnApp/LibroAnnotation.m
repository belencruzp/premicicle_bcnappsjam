//
//  LibroAnnotation.m
//  CoreLocationTutorial
//
//  Created by gJurado on 12/11/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "LibroAnnotation.h"

@implementation LibroAnnotation 

@synthesize coordinate = _coordinate;
@synthesize title = _title;
@synthesize subtitle = _subtitle;


-(id)init
{
    if(self = [super init])
    {
        //
    }
    return self;
}

-(id)initWithDictionary:(NSDictionary *)dict
{
    if(self = [super init])
    {
        _title = [dict  objectForKey:@"nombre"];
        _subtitle = [dict  objectForKey:@"descripcion"];// substringToIndex:15];
        _coordinate.latitude = [[dict objectForKey:@"latitude"]doubleValue];
        _coordinate.longitude = [[dict objectForKey:@"longitude"]doubleValue];
    }
    return self;
    
}




@end
