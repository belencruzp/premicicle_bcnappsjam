//
//  BibliotecaAnnotation.m
//  BiBcnApp
//
//  Created by gJurado on 12/15/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "BibliotecaAnnotation.h"

@implementation BibliotecaAnnotation


@synthesize title = _title;
@synthesize subtitle = _subtitle;
@synthesize coordinate = _coordinate;

-(id)init
{
    if(self = [super init])
    {
        //
    }
    return self;
}

-(id)initWithDictionary:(NSDictionary *)dict
{
    if(self = [super init])
    {
        _title = [dict  objectForKey:@"nombre"];
        _subtitle = [dict  objectForKey:@"direccion"];// substringToIndex:15];
        _coordinate.latitude = [[dict objectForKey:@"latitud"]doubleValue];
        _coordinate.longitude = [[dict objectForKey:@"longitud"]doubleValue];
    }
    return self;
    
}



@end
