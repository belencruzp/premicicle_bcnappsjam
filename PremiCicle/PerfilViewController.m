//
//  PerfilViewController.m
//  PremiCicle
//
//  Created by Giovanni Jurado on 6/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PerfilViewController.h"

@implementation PerfilViewController
@synthesize imagenPerfil;
@synthesize nombreUsuario_lbl;
@synthesize localizacion_lbl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [self loadFonts];
    [super viewDidLoad];
}


- (void)viewDidUnload
{
    [self setNombreUsuario_lbl:nil];
    [self setLocalizacion_lbl:nil];
    [self setImagenPerfil:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}   - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)loadFonts
{
    //Custom font donwloaded from the web. Inserted into info.plist as an array of "fonts provided by applications"
    [nombreUsuario_lbl setFont:[UIFont fontWithName:@"Questrial-Regular" size:70]]; 
    [nombreUsuario_lbl setTextColor:[UIColor whiteColor]];
    
    [localizacion_lbl setFont:[UIFont fontWithName:@"Questrial-Regular" size:55]];
    [localizacion_lbl setTextColor:[UIColor whiteColor]];
}

@end
