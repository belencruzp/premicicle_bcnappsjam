//
//  localiza.h
//  PremiCicle
//
//  Created by Giovanni Jurado on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface localizaViewController : UIViewController < CLLocationManagerDelegate, MKMapViewDelegate>


@property (weak, nonatomic) IBOutlet MKMapView *mapaPuntosVerdes;
@property (nonatomic) int tipoAnnotation;
@property (nonatomic) CLLocationCoordinate2D coordenadaBusqueda;



- (void) setMapaInicial;


@end
