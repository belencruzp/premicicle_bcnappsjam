//
//  PremiCicleViewController.h
//  PremiCicle
//
//  Created by Giovanni Jurado on 6/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PremiCicleViewController : UIViewController <UIScrollViewDelegate>
{
    BOOL pageControlIsChangingPage;
}
@property (weak, nonatomic) IBOutlet UIScrollView *tutorialScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *tutorialPageControl;
@property (weak, nonatomic) IBOutlet UIImageView *TutorialImagen;


- (IBAction)changePage:(id)sender;
- (void)setupPage;



@end
