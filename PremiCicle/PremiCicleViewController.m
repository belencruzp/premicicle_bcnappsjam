//
//  PremiCicleViewController.m
//  PremiCicle
//
//  Created by Giovanni Jurado on 6/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PremiCicleViewController.h"

@implementation PremiCicleViewController

@synthesize tutorialScrollView;
@synthesize tutorialPageControl;
@synthesize TutorialImagen;


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupPage];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setTutorialScrollView:nil];
    [self setTutorialPageControl:nil];
    [self setTutorialImagen:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark PageControl

- (IBAction)changePage:(id)sender 
{
	/*
	 *	Change the scroll view
	 */
    CGRect frame = tutorialScrollView.frame;
    frame.origin.x = frame.size.width * tutorialPageControl.currentPage;
    frame.origin.y = 0;
	
    [tutorialScrollView scrollRectToVisible:frame animated:YES];
    
	/*
	 *	When the animated scrolling finishings, scrollViewDidEndDecelerating will turn this off
	 */
    pageControlIsChangingPage = YES;
}

#pragma mark The Guts
- (void)setupPage
{
	tutorialScrollView.delegate = self;
    
	[self.tutorialScrollView setBackgroundColor:[UIColor clearColor]];
    tutorialScrollView.contentSize = CGSizeMake(320,320);
                                                //tutorialScrollView.contentSize.width,tutorialScrollView.frame.size.height);
	[tutorialScrollView setCanCancelContentTouches:NO];
	

	tutorialScrollView.clipsToBounds = YES;
	tutorialScrollView.scrollEnabled = YES;
	tutorialScrollView.pagingEnabled = YES;
	
	NSUInteger nimages = 0;
	CGFloat cx = 0;
	for (; ; nimages++) {
		NSString *imageName = [NSString stringWithFormat:@"tuto%d.png", (nimages + 1)];
		UIImage *image = [UIImage imageNamed:imageName];
		if (image == nil) {
			break;
		}
		UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
		
		CGRect rect = imageView.frame;
		rect.size.height = image.size.height;
		rect.size.width = image.size.width;
		rect.origin.x = ((tutorialScrollView.frame.size.width - image.size.width) / 2) + cx;
		rect.origin.y = ((tutorialScrollView.frame.size.height - image.size.height) / 2);
        
		imageView.frame = rect;
        
		[tutorialScrollView addSubview:imageView];
        
		cx += tutorialScrollView.frame.size.width;
	}
	
	self.tutorialPageControl.numberOfPages = nimages;
	[tutorialScrollView setContentSize:CGSizeMake(cx,[tutorialScrollView bounds].size.height)]; 
            
}

#pragma mark UIScrollViewDelegate stuff
- (void)scrollViewDidScroll:(UIScrollView *)_scrollView
{
        if (pageControlIsChangingPage) {
            return;
        }
    
	/*
	 *	We switch page at 50% across
	 */
        CGFloat pageWidth = _scrollView.frame.size.width;
        int page = floor((_scrollView.contentOffset.x - pageWidth / 4) / pageWidth) + 1;
        tutorialPageControl.currentPage = page;
    
    
    [tutorialScrollView setContentOffset: CGPointMake(tutorialScrollView.contentOffset.x, 0)]; //Bloquea el desplazamiento vertical
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)_scrollView 
{
    pageControlIsChangingPage = NO;
}



@end
