//
//  PremiCicleAppDelegate.h
//  PremiCicle
//
//  Created by Giovanni Jurado on 6/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PremiCicleAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
