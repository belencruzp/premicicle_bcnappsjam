//
//  PerfilViewController.h
//  PremiCicle
//
//  Created by Giovanni Jurado on 6/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PerfilViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *imagenPerfil;
@property (weak, nonatomic) IBOutlet UILabel *nombreUsuario_lbl;
@property (weak, nonatomic) IBOutlet UILabel *localizacion_lbl;

- (void)loadFonts;



@end
