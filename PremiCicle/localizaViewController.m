//
//  localiza.m
//  PremiCicle
//
//  Created by Giovanni Jurado on 6/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "localizaViewController.h"

@implementation localizaViewController
@synthesize mapaPuntosVerdes, tipoAnnotation, coordenadaBusqueda;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setMapaInicial];
   
}


- (void)viewDidUnload
{
    [self setMapaPuntosVerdes:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)setMapaInicial
{
    self.mapaPuntosVerdes.delegate = self;
    MKCoordinateRegion region; 
    
    double minLat = 41.412286;
    double maxLat = 41.41602;
    double minLon = 2.175021;
    double maxLon = 2.216563;
    
    region.center.latitude = (maxLat + minLat)/2;
    region.center.longitude = (maxLon + minLon)/2;
    region.span.latitudeDelta = maxLat - minLat;
    region.span.longitudeDelta = maxLon - minLon;
    
    [self.mapaPuntosVerdes setRegion:region animated:YES];
}

@end
