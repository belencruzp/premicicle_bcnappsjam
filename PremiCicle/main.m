//
//  main.m
//  PremiCicle
//
//  Created by Giovanni Jurado on 6/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PremiCicleAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PremiCicleAppDelegate class]));
    }
}
